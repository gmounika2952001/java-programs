

        import java.util.Scanner;
public class ArrayUsingFunction {

    public int typeOfArray(int[] a)
    {
        int even_cou = 0, odd_cou = 0;
        for (int i = 0;i < a.length; i++)
        {
            if (a[i] % 2 == 0 )
                even_cou += 1;
            else
                odd_cou += 1;

        }
        int size = a.length;
        if (even_cou==size)
            return 1;
        else if(odd_cou==size)
            return 2;
        else
            return 3;
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the array range");
        int n = sc.nextInt();
        int[] arr = new int[n];
        System.out.println("Enter the elements of the array ::");

        for(int i=0; i<n; i++) {
            arr[i] = sc.nextInt();
        }
        ArrayUsingFunction  ar= new ArrayUsingFunction();
        int t;
        t=ar.typeOfArray(arr);
        if(t==1)
            System.out.println("Even");
        else if(t==2)
            System.out.println("Odd");
        else
            System.out.println("Mixed");


    }
}
